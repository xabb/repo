#!/bin/bash
# FABRIQUEM UN FONS BLANC DE LA MIDA DE LA CAMPANYA
convert -size 923x1200 xc:white FONS_BLANC.png

convert \
-font helvetica -fill black -pointsize 35 -draw "text 50,100 'Formació'" \
-font helvetica -fill black -pointsize 35 -draw "text 50,140 'Professional'" \
-draw "line 50,150 240,150" \
-draw "line 50,151 240,151" \
-draw "line 50,152 240,152" \
-draw "line 50,153 240,153" \
-draw "line 50,154 240,154" \
-draw "line 50,155 240,155" \
-font helvetica -fill black -pointsize 150 -draw "text 620,180 '3%'"  \
FONS_BLANC.png output1.png # ESPECIFIQUEM SOBRE QUIN ARXIU/MODEL ESCRIBIM I EXPORTEM
