#!/bin/bash
# define usage function
usage(){
	echo "###########################################################################"
	echo "bash $0 URL nom_tema"
	echo "[exemple]$ bash $0 URL nom_tema https://www.youtube.com/watch?v=hbpUfWz-rlc pearl-jam_better-man"
	exit 1
}

if [ $# -ne 2 ]; then
	echo "###########################################################################"
	echo "NECESSITO 2 ARGUMENTS, la URL de youtube i per desar-lo, un nom SENSE ESPAIS"
	usage    
	exit 1
fi

youtube-dl -x --audio-format mp3 "$1" -o "$2"-YTD.mp3 

## https://pypi.org/project/telegram-send/
## sudo pip3 install telegram-send
## Please run: telegram-send --configure
## Talk with the BotFather on Telegram (https://telegram.me/BotFather), create a bot and insert the token
## 
telegram-send -f "$2"-YTD.mp3
